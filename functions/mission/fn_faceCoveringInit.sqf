/*
	SXP_fnc_faceCoveringInit
	
	Initializes the face covering system
	Executed in postInit
*/

if (!hasInterface) exitWith {};

private _action = ["SXP_goggle_toggle", "Toggle face covering", "", {[_player] call SXP_fnc_faceCovering},{true}] call ace_interact_menu_fnc_createAction;
["Man", 1, ["ACE_SelfActions","ACE_Equipment"], _action, true] call ace_interact_menu_fnc_addActionToClass;

/*
["ace_interact_menu_newControllableObject", {
	params ["_type"]; // string of the object's classname
	if (!(_type isKindOf "Man")) exitWith {};
	//if ((getNumber (configFile >> "CfgVehicles" >> _type >> "side")) != 3) exitWith {};
	
	private _action = ["SXP_goggle_toggle", "Toggle face covering", "", {[_player] call SXP_fnc_faceCovering}] call ace_interact_menu_fnc_createAction;
	[_type, 1, ["ACE_SelfActions","ACE_Equipment"], _action, true] call ace_interact_menu_fnc_addActionToClass;
}] call CBA_fnc_addEventHandler;
*/