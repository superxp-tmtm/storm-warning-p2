/*
	SXP_fnc_faceCovering
	
	Handles covering or uncovering the players face to protect them from dust.
*/

// "lxWS_H_turban_03_black" regexMatch "lxWS_H_turban_[0-9][0-9].*"
// "lxWS_H_turban_03_black" regexReplace ["(lxWS_H_turban_)(\d{2})(.*)", "\102\3"] 

params ["_unit"];

private _headGear = headgear _unit;
// Check if the headgear is one of the WS turbans
if !(_headGear regexMatch "lxWS_H_turban.*") exitWith {};

if (isNil "SXP_goggleLayer") then {
	missionNameSpace setVariable ["SXP_goggleLayer", ["SXP_goggleLayer"] call BIS_fnc_rscLayer];
};

// Check what kind of turban it is:
private _type = parseNumber ((_headGear regexFind ["\d{2}"])#0#0#0);
private _goggleLayer = missionNamespace getVariable "SXP_goggleLayer";

if (_type == 3) then {
	// Full face turban
	_unit linkItem (_headGear regexReplace ["(\d{2})", "02"]);
	_unit unlinkItem (goggles _unit);
	
	_goggleLayer cutText ["", "PLAIN", 1, false];
	
	
} else {
	// Other turban
	_unit linkItem (_headGear regexReplace ["(\d{2})", "03"]);
	_unit linkItem "G_Combat_lxWS";
	
	_goggleLayer cutRsc ["RscSXP_GoggleOverlay", "PLAIN", 1, false];
	((uiNameSpace getVariable "RscSXP_GoggleDisplay") displayCtrl 20101) ctrlSetText "media\DivingGoggles.paa";
};

// goggleLayer = ["SXP_goggleLayer"] call BIS_fnc_rscLayer;
// goggleLayer cutRsc ["RscSXP_GoggleOverlay", "PLAIN", 1, false];
// ((uiNameSpace getVariable "RscSXP_GoggleDisplay") displayCtrl 20101) ctrlSetText "media\DivingGoggles.paa";