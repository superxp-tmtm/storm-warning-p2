/*
	SXP_fnc_stormPostProcess
	
	Handles applying postprocess effects when the storm changes intensity
	
	Params: 0 - Number: Intensity of Storm
*/
if (!hasInterface) exitWith {};

params [
	["_intensity",missionNamespace getVariable ["SXP_desertStorm_intensity", 0],[0]],
	["_unit", player, [objNull]],
	["_onRespawn", false, [true]]
];

if (isNil "sxp_ppEffect_storm_colour") then {
	sxp_ppEffect_storm_colour = ppEffectCreate ["colorCorrections", 2551];
	sxp_ppEffect_storm_colour ppEffectEnable true;
};
if (isNil "sxp_ppEffect_storm_grain") then {
	sxp_ppEffect_storm_grain = ppEffectCreate ["FilmGrain", 2552];
	sxp_ppEffect_storm_grain ppEffectEnable true;
};

private _commitTime = if (_onRespawn) then {0} else {15};

/*
// Effects at intensity 1:
sxp_desertStorm ppEffectAdjust [1,1,0,[0.1,0.2,0.3,-0.5],[1.1,0.7,0.4,0.4],[0.5,0.2,0,1]];
sxp_desertStorm ppEffectCommit 1;

sxp_desertStorm_FilmGrain ppEffectAdjust [0.1,1,1,0,1];

// Intensity 0
sxp_desertStorm ppEffectAdjust [1,1,0,[0.1,0.2,0.3,-0.5],[1,1,1,0.4],[0.5,0.2,0,1]];
sxp_desertStorm ppEffectCommit 1;

sxp_desertStorm_FilmGrain ppEffectAdjust [0,1,1,0,1];
*/

// This is going to use a lot of LinearConversions
sxp_ppEffect_storm_colour ppEffectAdjust [
	1, // Brightness
	1, // Contrast
	0, // Offset
	[0.1,0.2,0.3,-0.5], // Blending Colour
	[
		linearConversion [0,1,_intensity,1,1.1,true], // Red
		linearConversion [0,1,_intensity,1,0.7,true], // Green
		linearConversion [0,1,_intensity,1,0.4,true], // Blue
		0.4 // Alpha
	], // Colorization
	[0.5,0.2,0,1] // Desaturation
];

sxp_ppEffect_storm_grain ppEffectAdjust [
	linearConversion [0,1,_intensity,0,0.15,true], // Intensity
	1, // Sharpness
	1, // Grainsize
	0, // Intensity x0
	1 // Intensity x1
];

// Set radio transmission range
if (!isNull _unit) then {
	_unit setVariable ["tf_sendingDistanceMultiplicator", linearConversion [0,1,_intensity,1,0.05,true]];
};

[sxp_ppEffect_storm_colour,sxp_ppEffect_storm_grain] ppEffectCommit _commitTime;