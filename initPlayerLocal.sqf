// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Uncomment the line below to use briefing.sqf for mission briefings. Un-needed if you're using XPTBriefings.hpp
//[] execVM "scripts\briefing.sqf";

// Initial Post-Processing Effects
sxp_ppEffect_desert = ppEffectCreate ["colorCorrections", 2501];
sxp_ppEffect_desert ppEffectEnable true;
sxp_ppEffect_desert ppEffectAdjust [1,1.02,-0.005,[0,0,0,0],[1,0.8,0.6,0.65],[0.199,0.587,0.115,0]];
sxp_ppEffect_desert ppEffectCommit 0;


//