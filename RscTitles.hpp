class RscTitles {
	// Control types
	#define CT_STATIC           0
	#define ST_PICTURE        0x30

	////////////////
	//Base Classes//
	////////////////

	class RscPicture {
		idc = -1;
		type = CT_STATIC;
		style = ST_PICTURE;
		colorBackground[] = {0,0,0,0};
		colorText[] = {1,1,1,1};
		font = "RobotoCondensed";
		sizeEx = 0;
		lineSpacing = 0;
		fixedWidth = 0;
		shadow = 0;
		text = "";
		x = safezoneX;
		y = safezoneY;
		w = safezoneW;
		h = safezoneH;
	};
		
	class RscSXP_GoggleOverlay_Base {
		idd = -1;
		name = "RscSXP_GoggleOverlayBase";
		onLoad = "uiNamespace setVariable ['RscSXP_GoggleDisplay', _this select 0]";
		onUnload = "uiNamespace setVariable ['RscSXP_GoggleDisplay', displayNull]";
		fadeIn = 0.5;
		fadeOut = 0.5;
		movingEnable = 0;
		duration = 10e10;
		class controls;
	};
	class RscSXP_GoggleOverlay: RscSXP_GoggleOverlay_Base {
		idd = 20100;
		scriptName = "RscSXP_GoggleOverlay";
		name = "RscSXP_GoggleOverlay";
		class controls {
			class gogglesImage: RscPicture {
				idc = 20101;
			};
		};
	};
};